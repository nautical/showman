var User = require('../models/user');
var TopNews = require('../models/topnews');
var Examinations = require('../models/examinations');
var subs = require('../models/subs');
var config = require('../config.json');
var crypto = require('crypto');
var bCrypt = require('bcrypt-nodejs');
var app = require('../app');
var sections = require('../config/sections.json');

var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

var createHash = function(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


module.exports.updateUser = function(req,res){
	User.findOne(
				{ 'username' :  req.user.username },
				function (err, user) {
					console.log(user);
				  	user.firstName=req.body.fname ? req.body.fname : req.user.firstName;
				  	user.lastName=req.body.lname ? req.body.lname : req.user.lastName;
				  	user.address=req.body.address ? req.body.address : req.user.address;
				  	if(req.body.p1 && req.body.p2){
				  		if(req.body.p1===req.body.p2){
				  			user.password=createHash(req.body.p1);
				  			req.flash('message', 'Password Updated');
				  		}
				  	}
				  	else{
					  	if(req.body.p1 || req.body.p2){
				  			req.flash('message', 'Both password should match');
					  	}
				  	}
				  	user.save(function (err) {
					    if(err) {
					    	req.flash('message', 'Error in saving, please try again');
					    	res.redirect('/profile');
					    }
					    else{
					    	req.flash('message', 'Profile updated');
					    	res.redirect('/profile');
					    }
					});
				}
			);
}



module.exports.showtops = function(req, res){
		TopNews.find({}, function (err, docs) {
    		res.render('top', { user: req.user , news:docs , activetab: "top"});
    	});
}

module.exports.activateuser = function(req,res){
	var userId = req.params.id;
	var code = req.params.code;
	User.findOne( 
		{'_id' : userId},
		function(err,user){
			if(user.verification_code==code){
				user.is_verified = true;
				user.save(function(err){
					if(err){
						console.log("Error updating verification code");
					}
					else{
						req.flash('message', 'Account verified please login');
					    res.redirect('/');
					}
				});
			}
			else{
				req.flash('message','Error in link , please try again');
			}
		
		});
}

module.exports.forgotpassword = function(req,res){
	var username = req.params.username;
	User.findOne(
		{'username': username},
		function(err,user){
			if(err){
				res.json({"status":"error","reason":"User not found"})
			}
			if(user){
				var kypt = crypto.randomBytes(10).toString('hex')
				user.pass_verification = kypt;
				user.save(function(err,user) {
                            if (err){
                                console.log('Error in Saving user: '+err);  
                                res.json({"status":"error","reason":"Please try again in some time"})
                            }
                            else{
                                app.mailer.send(
                                      {
                                        template: 'mail/passwordreset'
                                      },
                                      {
                                        to: user.email,
                                        subject: 'Password Reset link for IBPS.how',
                                        resetlink: 'http://exam.ibps.how/resetpassword/'+user._id+'/'+kypt
                                      },
                                      function (err) {
                                        if (err) {
                                          console.log("error sending email" + err + " > " + user._id); 
                                        };
                                          console.log("mail sent");
                                      }
                                );
                            }
                            res.json({"status":"success"})
                        });
			}
			else{
				res.json({"status":"error","reason":"User not found"})
			}
	})
}


module.exports.resetpassword = function(req,res){
	var userID = req.params.id;
	var code = req.params.code;
	User.findOne(
		{'_id' : userID },
		function(err,user){
			if(err){
				console.log("error : " + err);
			}
			if(user){
				res.render('forgotpassword', { userid: userID , code : code });
			}
			else{
				req.flash('message','Error in password reset link');
				res.redirect("/");
			}
		});
}

module.exports.post_resetpassword = function(req,res){
	var pass1 = req.body.pass1;
	var pass2 = req.body.pass2;
	var user = req.body.userID;
	var code = req.body.vercode;
	if(pass1==pass2){
		User.findOne(
			{'_id':user},
			function(err,user){
				if(user.pass_verification==code){
					user.password=createHash(pass1);
					user.pass_verification = crypto.randomBytes(12).toString('hex') // change code once verified ..
					user.is_verified = true
					user.save(function(err,user){
						if(err){
							req.flash('message','Error saving new password , please try again');
						}
						if(user){
							req.flash('message','Password has been reset , please login with new password');
						}
						res.redirect('/');
					})
				}
				else{
					console.log(user.pass_verification);
					console.log(code);
					console.log(user);
					req.flash('message','Error in resetting password , please try again');
					res.redirect('/');
				}
		});
	}
	else{
		req.flash('message','Error in resetting password , Both password should match');
		res.redirect('/');
	}
}

module.exports.renderhome = function(req, res){
		var startdate = new Date(req.user.activation_date);
		var number_of_exams = config.plans[""+req.user.ibps_accnt_type];
		Examinations.find( {$and:[ {date: { $gt : startdate } } ,{isactive : true} ]}, 'date examID result_announced numberofstudents total_topscore quant_avg reasoning_avg english_avg cs_avg ga_avg').sort({date: 1}).limit(number_of_exams).exec(
		function(err, info) {
				var remaining = 0 ;
				var nxt = "0";
				var bool_examgiven = false;
				var chk = 0;
				var id;
				console.log(req.user);
				for(var inf in info){
					if(info[inf].date > new Date()){
						if(chk==0) {
							var tmp = new Date(info[inf].date);
							for(test in req.user.user_test_history){
								if(new Date(req.user.user_test_history[test].date).getTime() == new Date(info[inf].date).getTime()){
									bool_examgiven = true;
								}
							}
							nxt = tmp.getDate() + " " + month[tmp.getMonth()];
							id = info[inf]._id;
							chk = 1;
						}
						remaining++;
					}
					req.user.user_test_history.forEach(function(i,j){ 
						if(i.date.getTime()==info[inf].date.getTime()) {
							info[inf].user_score = i.score ;
							info[inf].user_quant_score = i.quant_score ;
							info[inf].user_reasoning_score = i.reasoning_score ;
							info[inf].user_english_score = i.english_score ;
							info[inf].user_cs_score = i.cs_score ;
							info[inf].user_ga_score = i.ga_score ;
						}
						
					})
				}
				res.render('home', { 
					user: req.user , 
					count : remaining , 
					nextdate : nxt , 
					examinfo : info , 
					activetab: "home", 
					eID: id , 
					exam_given : bool_examgiven
				});
			}
		);
}

module.exports.renderAna = function(req, res){
		var startdate = new Date(req.user.activation_date);
		var number_of_exams = config.plans[""+req.user.ibps_accnt_type] ;
		var overall_perf = [];
		var quant_perf = [];
		var reasoning_perf = [];
		var eng_perf = [];
		var cs_perf = [];
		var ga_perf = [];

		Examinations.find({date: { $gt : startdate } } , 'date numberofstudents total_cutoff quant_cutoff reasoning_cutoff english_cutoff cs_cutoff ga_cutoff total_topscore quant_avg reasoning_avg english_avg cs_avg ga_avg').sort({date: 1}).limit(number_of_exams).exec(
		function(err, info) {
				var chk = 0;
				for(var inf in info){
					req.user.user_test_history.forEach(function(i,j){ 
						var timestring = info[inf].date.getFullYear()+"-"+(info[inf].date.getMonth()+1)+"-"+info[inf].date.getDate()
						if(i.date.getTime()==info[inf].date.getTime()) {
							overall_perf.push({
								"period" : timestring ,
								"cutoff" : info[inf].total_cutoff ? info[inf].total_cutoff : 0,
								"avg" : info[inf].total_topscore ? info[inf].total_topscore: 0 ,
								"personal" : i.score ? i.score : 0
							});
							quant_perf.push({
								"period" : timestring ,
								"cutoff" : info[inf].quant_cutoff ? info[inf].quant_cutoff : 0 ,
								"avg" : info[inf].quant_avg ? info[inf].quant_avg : 0 ,
								"personal" : i.quant_score ? i.quant_score : 0
							});
							reasoning_perf.push({
								"period" : timestring ,
								"cutoff" : info[inf].reasoning_cutoff ? info[inf].reasoning_cutoff : 0,
								"avg" : info[inf].reasoning_avg ? info[inf].reasoning_avg : 0 ,
								"personal" : i.reasoning_score ? i.reasoning_score : 0
							});
							eng_perf.push({
								"period" : timestring ,
								"cutoff" : info[inf].english_cutoff ? info[inf].english_cutoff : 0,
								"avg" : info[inf].english_avg ? info[inf].english_avg : 0 ,
								"personal" : i.english_score ? i.english_score : 0
							});
							cs_perf.push({
								"period" : timestring ,
								"cutoff" : info[inf].cs_cutoff ? info[inf].cs_cutoff : 0,
								"avg" : info[inf].cs_avg ? info[inf].cs_avg : 0 ,
								"personal" : i.cs_score ? i.cs_score : 0
							});
							ga_perf.push({
								"period" : timestring ,
								"cutoff" : info[inf].ga_cutoff ? info[inf].ga_cutoff : 0,
								"avg" : info[inf].ga_avg ? info[inf].ga_avg : 0 ,
								"personal" : i.ga_score ? i.ga_score : 0
							});
						}
					})
				}
				res.render('analysis', { user: req.user, perf: [overall_perf,quant_perf,reasoning_perf,eng_perf,cs_perf,ga_perf] , activetab: "analysis"});
			}
		);
}

var gettitle = function(config,key){
	var name = ""
	for(var p in config){
		console.log(p)
		for(var q in config[p]){
			if(config[p][q]==key){
				return q
				break
			}
		}
	}
}

module.exports.getStaticQ = function(req,res){
	res.render('static', { user: req.user, activetab: "sectional" , subjectid : req.params.id , papername: gettitle(sections,req.params.id) });
}

module.exports.postStaticQ = function(req,res){
	var obj = JSON.parse(req.body.json);
	var key = Object.keys(obj)[0];
	var val = obj[key];

	User.findOne({'_id': req.user._id} , function(err,user){
		if(err){
			res.json({status:"error"});
		}
		if(user){
			if(user.user_sectional_history){
				user.user_sectional_history[key]=val;
				user.markModified('user_sectional_history');
			}
			else{
				user.user_sectional_history = {}
				user.user_sectional_history[key]=val;
			}
			
			user.save(function(err){
				if(err){
					console.log("eror in saving" + err);
					res.json({status:"error"})
				}
				else{
					res.json({status:"success"});
				}
			})
		}
		else{
			console.log("User not found ..");
			res.json({status:"error"});
		}
	})
}


module.exports.getStaticQD = function(req,res){
	res.render('static_view', { user: req.user, activetab: "sectional" , config: sections });
}

module.exports.getTools = function(req,res){
	res.render('tools', { user: req.user, activetab: "tools" });
}

module.exports.resendVeri = function(req,res){
	var userId = req.params.id;
	var enc = req.params.enc;

	User.findOne( 
		{'_id' : userId},
		function(err,user){

			app.mailer.send(
                  {
                    template: 'mail/singup'
                  },
                  {
                    to: user.email,
                    subject: 'Welcome to IBPS.how',
                    verificationlink: 'http://exam.ibps.how/activateuser/'+user._id+'/'+user.verification_code
                  },
                  function (err) {
                    if (err) {
                      console.log("error sending email" + err + " > " + newUser._id); 
                    }
                    else{
                    	console.log("mail sent");
                    	req.flash('message', 'Account verified please login');
						res.redirect('/');
                    }
                  }
            );
		});
}


module.exports.renderSpecial = function(req,res){

	var startdate = new Date(req.user.activation_date);
	var number_of_exams = config.plans[""+req.user.ibps_accnt_type];
	Examinations.find( {$and:[ {date: { $gt : startdate } } ,{isactive : true} , {examID: "SBI"} ]}, 'date examID result_announced numberofstudents total_topscore quant_avg reasoning_avg english_avg cs_avg ga_avg').sort({date: 1}).limit(number_of_exams).exec(
		function(err, info) {
				var remaining = 0 ;
				var nxt = "0";
				var bool_examgiven = false;
				var chk = 0;
				var id;
				console.log(req.user);
				for(var inf in info){
					if(info[inf].date > new Date()){
						if(chk==0) {
							var tmp = new Date(info[inf].date);
							for(test in req.user.user_test_history){
								if(new Date(req.user.user_test_history[test].date).getTime() == new Date(info[inf].date).getTime()){
									bool_examgiven = true;
								}
							}
							nxt = tmp.getDate() + " " + month[tmp.getMonth()];
							id = info[inf]._id;
							chk = 1;
						}
						remaining++;
					}
					req.user.user_test_history.forEach(function(i,j){ 
						if(i.date.getTime()==info[inf].date.getTime()) {
							info[inf].user_score = i.score ;
							info[inf].user_quant_score = i.quant_score ;
							info[inf].user_reasoning_score = i.reasoning_score ;
							info[inf].user_english_score = i.english_score ;
							info[inf].user_cs_score = i.cs_score ;
							info[inf].user_ga_score = i.ga_score ;
						}
						
					})
				}
				res.render('special', { 
					user: req.user , 
					count : remaining , 
					nextdate : nxt , 
					examinfo : info , 
					activetab: "home", 
					eID: id , 
					exam_given : bool_examgiven,
					activetab: "special"
				});
			}
		);
}

module.exports.renderquiz = function(req, res) {
		var bool_examgiven = false;
		Examinations.findOne({ '_id' :  req.params.id }, function(err, exam) {
			if(err){
				req.flash('message','Error in Exam ID');
				res.redirect('/home');
			}
			if(exam){
				if(exam.isactive){
				for(test in req.user.user_test_history){
					if(new Date(req.user.user_test_history[test].date).getTime() == new Date(exam.date).getTime()){
						bool_examgiven = true;
					}
				}
				if(bool_examgiven)
					res.render('notify', { user: req.user , usermessage : "It seems that you have given the exam , please check analytics to see your score as compared to others." , msgtype : "error" , activetab: "" });
				else
					res.render('quiz', { user: req.user , e : exam , Qid : req.params.id });
				}
				else{
					res.send("exam is not active")
				}
			}
			else{
				req.flash('message','Error in Exam ID');
				res.redirect('/home');
			}
		});
}

module.exports.quizapi = function (req,res) {
		Examinations.findOne({ '_id' :  req.params.id }, function(err, exam) {
			if(exam.isactive){
				var subject = req.params.subject;
				var ind = req.params.ind;
				var exobj = exam[subject];
				if(exobj){
					if(exobj[ind].qnum == ind){
						if(exobj[ind].q_major.indexOf("#same")>=0){
							var sameindex = parseInt(exobj[ind].q_major.split("#same")[1]);
							exobj[ind].q_major = exobj[sameindex-1].q_major;
						}
						res.json({"Q" : exobj[ind]}); // most cases should fall here 
					}
					else{
						for (var q in exobj){
						if(exobj[q].qnum == ind){
							if(exobj[ind].q_major.indexOf("#same")>=0){
								var sameindex = parseInt(exobj[ind].q_major.split("#same")[1]);
								exobj[ind].q_major = exobj[sameindex-1].q_major;
							}
							res.json({"Q" : exobj[q]});
						}
						}
					}
				}
				else{
					res.json({"Q" : "subject error"});
				}
			}
			else
				res.send("exam is not active")
		});
}

module.exports.quesNum = function (req,res) {
	Examinations.findOne({ '_id' :  req.params.id }, function(err, exam) {
			if(exam.isactive){
						res.json({"ga": exam["ga"].length,"cs": exam["cs"].length,"english": exam["english"].length
						,"reasoning": exam["reasoning"].length,"quant": exam["quant"].length});
					
				}
			else
				res.send("exam is not active")
		});
}


// work of admin
module.exports.updatetops = function(req,res){
	TopNews.findOne({ 'url' :  req.body.url }, function(err, URL) {
                    if (err){
                        console.log('Error in Lookup: '+err);
                        //res.json({status: "error", reason: "error in lookup"});
                        res.redirect("/addtop");
                    }
                    if (URL) {
                        console.log('URL exists');
                        //res.json({status: "error" , reason : "url exists"});
                        res.redirect("/addtop");
                    } else {
                        var newURL = new TopNews();
                        newURL.url = req.body.url;
                        newURL.heading = req.body.heading;
                        newURL.hash = createHash(req.body.url);
                        newURL.tag = req.body.tag;
                        newURL.save(function(err) {
                            if (err){
                                console.log('Error in Saving top news: '+err);
                                //res.json({status: "error" , reason : "error in saving"});
                                res.redirect("/addtop");
                            }  
                            //res.json({status: "success"});
                            res.redirect("/addtop");
                        });
                    }
                });
}

// work of admin
module.exports.renderaddtop = function(req, res){
		TopNews.find({}, function (err, docs) {
			res.render('addtop', { user: req.user , news:docs , activetab: "addtop"});
    	});
}

// work of admin
module.exports.deletepost = function(req, res){
		TopNews.remove({'_id': req.params.id}, function (err) { // just to be double sure .. 
			if(err){
				console.log("Error while deleting post : " + req.params.id);
			}
			TopNews.find({}, function (err, docs) {
				res.redirect('/addtop');
	    	});
    	});
}

// work of admin
module.exports.deleteall = function(req, res){
		TopNews.remove({}, function (err) {
			if(err){
				console.log("Got error .. in removing ");
			}
			TopNews.find({}, function (err, docs) {
				res.redirect('/addtop');
	    	});
    	});
}

// work of admin
module.exports.addexamdate = function(req,res){
		console.log(req.body.examdate);
		Examinations.findOne({ 'date' : req.body.examdate } , function(err, DATE){
				if(err){
					console.log('Error in Lookup: '+err);
                    res.json({status: "error", reason: "error in lookup"});
				}
				if(DATE){
					console.log('EXAM exists');
                    res.json({status: "error" , reason : "Exam exists"});
				} else {
					var newExam = new Examinations();
					newExam.date = req.body.examdate;
					newExam.examID = "IBPS"; // only ibps now .. might be other tomorrow
					newExam.save(function(err) {
                            if (err){
                                console.log('Error in Saving exam date: '+err);
                                res.json({status: "error" , reason : "error in saving"});
                            }  
                            res.json({status: "success"});
                        });
				}


		});
		res.json({status : "success"});
}

module.exports.getresult = function(req,res){
	var quizID = req.params.id;
	var bool_examgiven = false;
	var userscore = 0;
	Examinations.findOne({ '_id' : quizID } , function(err, quiz){
		if(err){
			res.flash("message","Error getting Quiz");
			res.render('results', { user: req.user });
		}
		if(quiz){
			if(quiz.result_announced){
				for(test in req.user.user_test_history){
					if(new Date(req.user.user_test_history[test].date).getTime() == new Date(quiz.date).getTime()){
						userscore = req.user.user_test_history[test].score;
						ga_score = req.user.user_test_history[test].ga_score;
						cs_score = req.user.user_test_history[test].cs_score;
						english_score = req.user.user_test_history[test].english_score;
						reasoning_score = req.user.user_test_history[test].reasoning_score;
						quant_score = req.user.user_test_history[test].quant_score;
						bool_examgiven = true;
					}
				}
				if(bool_examgiven){
					var userresponses ;
					for(var i in req.user.user_response_history){
						if(req.user.user_response_history[i].date){
							if(req.user.user_response_history[i].date.getTime() == quiz.date.getTime()){
								userresponses = req.user.user_response_history[i].responses
								break
							}
						}
					}
					res.render('results', { user: req.user , quiz: quiz , Uanswers : userresponses, userscore: userscore , 
						subscore: [ga_score,cs_score,english_score,reasoning_score,quant_score] });
				}
				else{
					req.flash("message","Sorry , It seems you have missed the exam !")
					res.render('notify', { user: req.user , usermessage : "It seems that you have missed the exam date ! Sorry" , msgtype : "error" , activetab: "" });
				}
				
			}
			else{
				req.flash('message','Result has not been declared yet !');
				res.redirect('/');
			}
		}
		else{
			req.flash("message","Error getting Quiz");
			res.render('results', { user: req.user });
		}
	});
}

module.exports.subscriberAdd = function(req,res){
	if(validateEmail(req.body.email)){
		subs.findOne({email: req.body.email} , function(err,sub){
			if(err){
				res.json({status:"error", reason:"error in db"});
			}
			if(sub){
				res.json({status:"error",reason:"Already exist"});
			}
			else{
				var sub_ = new subs();
				sub_.email = req.body.email; 
				sub_.save(function(err) {
                        if (err){
                            console.log('Error in Saving exam date: '+err);
                            res.json({status: "error" , reason : "error in saving"});
                        }  
                        res.json({status: "success"});
                    });
				//res.json({status:"success"});
			}
		})
	}
	else{
		res.json({status:"error", reason:"not valid email"});
	}
}

module.exports.processquiz = function(req,res){
	var resposne = JSON.parse(req.body.json);
	var quizID = req.params.id;
	var score=0;
	var quant_score=0;
	var reasoning_score=0;
	var english_score=0;
	var cs_score=0;
	var ga_score=0;

	Examinations.findOne({ '_id' : quizID } , function(err, quiz){
			if(err){
				console.log('Error in Lookup: '+err);
                res.json({status: "error", reason: "error in lookup"});
			}
			else{
				var userresponsearray = {};
				for(var r in resposne){
					var subject = resposne[r].name;
					var index = resposne[r].num;
					var selected = parseInt(resposne[r].selected);
					var Qobj = quiz[subject];
					if (userresponsearray[subject]==undefined){
						userresponsearray[subject] = {}
						userresponsearray[subject][index]=selected;
					}
					else{
						userresponsearray[subject][index]=selected;
					}

					var inQ;

					if(Qobj[index].qnum == index){
						inQ = Qobj[index] // most cases should fall here 
					}
					else{
						for(var Q in Qobj){
							if(Qobj[Q].qnum == index){
								inQ = Qobj[Q];
								break;
							}
						}
					}

					if( ""+(selected+1) == ""+(inQ.correct)){
						score++;
						if(subject=="reasoning"){reasoning_score++}
						if(subject=="english"){english_score++}
						if(subject=="quant"){quant_score++}
						if(subject=="ga"){ga_score++}
						if(subject=="cs"){cs_score++}
					}

					res.json({status:"success"});
				}
				User.findByIdAndUpdate(
				    req.user._id,
				    {$push: {
					    	"user_test_history": {
					    	date:quiz.date, 
					    	examID: "IBPS",
					    	score: score,
					    	quant_score:quant_score,
					    	reasoning_score:reasoning_score,
					    	english_score:english_score,
					    	cs_score:cs_score,
					    	ga_score:ga_score
					    		},
					    	"user_response_history" : {
								date: quiz.date,
								responses : userresponsearray
							}
					    }
					},
				    {safe: true, upsert: true},
				    function(err, model) {
				        console.log(err);
				    }
				);
				var Totstudents = typeof quiz.numberofstudents !='undefined' ? quiz.numberofstudents:0;
				var newTot = Totstudents+1;
				quiz.numberofstudents=newTot;
				quiz.quant_avg = typeof quiz.quant_avg !='undefined'?(quiz.quant_avg*Totstudents+quant_score)/newTot:quant_score ;
				quiz.reasoning_avg = typeof quiz.reasoning_avg !='undefined'? (quiz.reasoning_avg*Totstudents+reasoning_score)/newTot : reasoning_score ;
				quiz.english_avg = typeof quiz.english_avg !='undefined'? (quiz.english_avg*Totstudents+english_score)/newTot : english_score ;
				quiz.cs_avg = typeof quiz.cs_avg !='undefined'? (quiz.cs_avg*Totstudents+cs_score)/newTot : cs_score ;
				quiz.ga_avg = typeof quiz.ga_avg !='undefined'? (quiz.ga_avg*Totstudents+ga_score)/newTot : ga_score ;
				quiz.total_topscore = typeof quiz.total_topscore !='undefined'? ( (quiz.total_topscore>score) ?quiz.total_topscore:score) : score
				//quiz.top_ten_scores = if(quiz.top_ten_scores) ? (for(item in )) : [score]
				quiz.save(function (err) {
			        if(err) {
			            console.error('ERROR!');
			        }
			    });
			}
	});
	//res.json({status:"success"});
}

// work of admin
module.exports.createtestQ = function(req, res){
		var questionpaper = req.body;
		var keys = Object.keys(questionpaper);
		var Darr = req.body.examdate.split("-");
		var newDate=Darr[0]+"/"+Darr[1]+"/"+Darr[2];
		var Ondate = new Date(newDate) ;
		
		var quant = [];
		var reasoning = [];
		var english = [];
		var cs = [];
		var ga = [];

		for(var key in keys){
			if(keys[key].indexOf("re_q1")>-1){
				var qnum = keys[key].replace("re_q1","");
				var major_q = req.body["re_q1"+qnum];
				var minor_q = req.body["re_q2"+qnum];
				var options = req.body["re_options"+qnum].split("::");
				var correct = req.body["re_correct"+qnum];
				var solution = req.body["re_solution"+qnum];
				var Qobject = {
					q_major: major_q,
					q_minor: minor_q,
					answer_options: options,
					correct: correct,
					solution: solution,
					score: 1,
					negative_marks: 0,
					qnum: parseInt(qnum)
				}
				reasoning.push(Qobject);
			}
			if(keys[key].indexOf("eng_q1")>-1){
				var qnum = keys[key].replace("eng_q1","");
				var major_q = req.body["eng_q1"+qnum];
				var minor_q = req.body["eng_q2"+qnum];
				var options = req.body["eng_options"+qnum].split("::");
				var correct = req.body["eng_correct"+qnum];
				var solution = req.body["eng_solution"+qnum];
				var Qobject = {
					q_major: major_q,
					q_minor: minor_q,
					answer_options: options,
					correct: correct,
					solution: solution,
					score: 1,
					negative_marks: 0,
					qnum: parseInt(qnum)
				}
				english.push(Qobject);
			}
			if(keys[key].indexOf("num_q1")>-1){
				var qnum = keys[key].replace("num_q1","");
				var major_q = req.body["num_q1"+qnum];
				var minor_q = req.body["num_q2"+qnum];
				var options = req.body["num_options"+qnum].split("::");
				var correct = req.body["num_correct"+qnum];
				var solution = req.body["num_solution"+qnum];
				var Qobject = {
					q_major: major_q,
					q_minor: minor_q,
					answer_options: options,
					correct: correct,
					solution: solution,
					score: 1,
					negative_marks: 0,
					qnum: parseInt(qnum)
				}
				quant.push(Qobject);
			}
			if(keys[key].indexOf("ga_q1")>-1){
				var qnum = keys[key].replace("ga_q1","");
				var major_q = req.body["ga_q1"+qnum];
				var minor_q = req.body["ga_q2"+qnum];
				var options = req.body["ga_options"+qnum].split("::");
				var correct = req.body["ga_correct"+qnum];
				var solution = req.body["ga_solution"+qnum];
				var Qobject = {
					q_major: major_q,
					q_minor: minor_q,
					answer_options: options,
					correct: correct,
					solution: solution,
					score: 1,
					negative_marks: 0,
					qnum: parseInt(qnum)
				}
				ga.push(Qobject);
			}
			if(keys[key].indexOf("cs_q1")>-1){
				var qnum = keys[key].replace("cs_q1","");
				var major_q = req.body["cs_q1"+qnum];
				var minor_q = req.body["cs_q2"+qnum];
				var options = req.body["cs_options"+qnum].split("::");
				console.log(options)
				var correct = req.body["cs_correct"+qnum];
				var solution = req.body["cs_solution"+qnum];
				var Qobject = {
					q_major: major_q,
					q_minor: minor_q,
					answer_options: options,
					correct: correct,
					solution: solution,
					score: 1,
					negative_marks: 0,
					qnum: parseInt(qnum)
				}
				cs.push(Qobject);
			}
		}
		var examObject = {
			date: Ondate,
			examID : "IBPS",
			isactive: false,
			quant: quant,
			reasoning: reasoning,
			english: english,
			cs: cs,
			ga: ga,
		}
		console.log(examObject);
		new Examinations(examObject).save( function( err, exm, count ){
			if(err)
				console.log("Error while saving exam");
			else
				res.redirect('/createtest');
		});
		//res.render('createtest', { user: req.user , activetab: "createtest"});
}

// work of admin 
module.exports.managetestQ = function(req,res){
	Examinations.find({} , function(err, exams){
		res.render('managetest', { user: req.user , exams:exams , activetab: "managetest"});
	});
}


//work of admin
module.exports.showQuestionstestQ = function(req,res){
	Examinations.findOne({'_id':req.params.id} , function(err, exam){

	});
}

// work of admin
module.exports.getQs = function(req,res){
	var examID = req.params.id;
	Examinations.findOne({'_id':examID}, function(err,exam){
		if(err){
			res.flash('message','Error in finding the requested Exam in DB');
			res.redirect('/home');
		}
		if(exam){
			res.render('getQs', {user: req.user , examreq: exam, activetab: "managetest"});
		}
		else{
			res.flash('message','Error in finding the requested Exam');
			res.redirect('/home');
		}
	})
}

module.exports.manageuser = function(req,res){
	User.find({},'email',function(err,users){
		if(err){
			console.log("Encountered error : " + err);
		}
		if(users){
			res.json(users);
		}
		else{
			res.json({"error":"DB error"});
		}
	}
	);
}


// work of admin 
module.exports.modtestQ = function(req,res){
	Examinations.findOne({'_id':req.params.id} , function(err, exam){
		if(err){
			res.flash('message','err in db');
			res.redirect('/managetest');
		}
		if(exam){
			exam.isactive = (req.body.is_active) ? (req.body.is_active.length>0 ? req.body.is_active : exam.isactive) : false ;
			exam.result_announced = (req.body.result_active) ? (req.body.result_active.length>0 ? req.body.result_active : exam.result_announced ) : false;
			exam.total_cutoff = req.body.totalcutoff.length>0 ? req.body.totalcutoff : exam.total_cutoff;
			exam.quant_cutoff = req.body.qa.length>0 ? req.body.qa : exam.quant_cutoff;
			exam.reasoning_cutoff = req.body.rea.length>0 ? req.body.rea : exam.reasoning_cutoff;
			exam.english_cutoff = req.body.eng.length>0 ? req.body.eng : exam.english_cutoff;
			exam.cs_cutoff = req.body.cs.length>0 ? req.body.cs : exam.cs_cutoff;
			exam.ga_cutoff = req.body.ga.length>0 ? req.body.ga : exam.ga_cutoff;
			exam.save(function(err){
				if(err){
					res.flash('message','Error saving');
				}
				res.redirect("/managetest");
			});
			// res.render('managetest', { user: req.user , exams:exams , activetab: "managetest"});
		}
		else{
			res.flash('message','error finding the exam');
			res.redirect('/managetest');
		}
	});
}


// work fo admin
module.exports.sendmailAdmin = function(req,res){
	
}



