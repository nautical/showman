
var mongoose = require('mongoose');

module.exports = mongoose.model('topnews',{
	id: String,
	url: String,
	heading: String,
	hash: String,
	tag: String
});