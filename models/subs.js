
var mongoose = require('mongoose');

module.exports = mongoose.model('subscribers',{
	id: String,
	email: String,
});