
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var user_test = new Schema({
// 	date: Date,
// 	examID: String,
// 	score: Number,
// 	quant_score: Number,
// 	reasoning_score: Number,
// 	english_score: Number,
// 	cs_score: Number,
// 	ga_score: Number
// });

// var user_response = new Schema({
// 	date: Date,
// 	responses: []
// });

module.exports = mongoose.model('User',{
	id: String,
	
	username: String,
	password: String,
	email: String,
	firstName: String,
	lastName: String,
	address: String,
	working_status: Boolean,
	
	// for the purpose of email verification
	verification_code: String,
	is_verified: Boolean,

	// password verification code (forgot password)
	pass_verification: String,

	user_test_history:[],//[user_test],
	user_response_history:[],// [user_response],
	user_sectional_history: {},
	
	avg_score: Number,
	avg_quant_score: Number,
	avg_reasoning_score: Number,
	avg_english_score: Number,
	avg_cs_score: Number,
	avg_ga_score: Number,
	
	ibps_accnt_type: String,
	activation_date: Date
});