
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var questionset = new Schema({
// 	q_major: String,
// 	q_minor: String,
// 	answer_options: [],
// 	correct: Number,
// 	score: Number,
// 	negative_marks: Number
// });


module.exports = mongoose.model('examinations',{
	id: String,

	isactive: Boolean, 
	result_announced: Boolean,
	//date of exam
	date: Date,
	examID : String, // currently only IBPS .. later might change to other values eg IAS

	//section wise questions
	quant: [],//[questionset],
	reasoning: [],//[questionset],
	english: [],//[questionset],
	cs: [],//[questionset],
	ga: [],//[questionset],

	/////////////////// somestats //////////////////////
	//number of students giving exam
	numberofstudents: Number,
	
	// Topper of exam
	total_topscore: Number,
	// Top 10 students will be shown on result page
	top_ten_scores: [],

	//subject wise toppers
	quant_topscore: Number,
	reasoning_topscore: Number,
	english_topscore: Number,
	cs_topscore: Number,
	ga_topscore: Number,

	// cutoff total
	total_cutoff: Number,
	// Subject wise cutoff
	quant_cutoff: Number,
	reasoning_cutoff: Number,
	english_cutoff: Number,
	cs_cutoff: Number,
	ga_cutoff: Number,
	
	// Subject wise average
	quant_avg: Number,
	reasoning_avg: Number,
	english_avg: Number,
	cs_avg: Number,
	ga_avg: Number,

});