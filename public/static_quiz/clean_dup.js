var _ = require('underscore');
var fs = require('fs');

function arrUnique(arr) {
    var cleaned = [];
    arr.forEach(function(itm) {
        var unique = true;
        cleaned.forEach(function(itm2) {
            if (_.isEqual(itm, itm2)) unique = false;
        });
        if (unique)  cleaned.push(itm);
    });
    return cleaned;
}

var fs = require('fs');
var obj = JSON.parse(fs.readFileSync(process.argv[2]))["quiz"];
console.log(obj.length)
var standardsList = arrUnique(obj);
console.log(standardsList.length)
var output = "{ \"quiz\": " + JSON.stringify(standardsList) + " }"
fs.writeFile("./cleaned/"+process.argv[2] , output, function(err) {
    if(err) {
        return console.log(err);
    }
    console.log(process.argv[2] + " cleaned .. ");
});