var subjects = ["reasoning","eng","quant","ga","cs"];

var reasoning=0
var eng=0
var quant=0
var ga=0
var cs=0

var indexTOapi = function(index){
    if(index-reasoning<=0){
        return "/reasoning/"+ (index-1);
    }
    if(index-(reasoning+eng)<=0){
        return "/english/" + (index-reasoning-1);
    }
    if(index-(reasoning+eng+quant)<=0){
        return "/quant/" + (index-reasoning-eng-1);
    }
    if(index-(reasoning+eng+quant+ga)<=0){
        return "/ga/" + (index-reasoning-eng-quant-1)
    }
    if(index-(reasoning+eng+quant+ga+cs)<=0){
        return "/cs/" + (index-reasoning-eng-quant-ga-1)
    }
}

var getQ = function(Qid,index,answers){
    if(index<=reasoning+eng+quant+ga+cs){
        var endpoint = indexTOapi(index);
        console.log(endpoint);
        $.getJSON( "/quiz/"+Qid+endpoint, function( data ) {
                var q_minor = data.Q.q_minor;
                var q_major = data.Q.q_major;
                var options = data.Q.answer_options;
                $('.q_minor').html(q_minor);
                $('.q_major').html("<p>Question Number : "+index+"</p><br>"+q_major);
                var pinpoint = endpoint.split("/");
                var options_content = "";
                for(var option in options){
                    options_content += '<div class="radio answerS col-lg-12 normal_text"><label><input overindex="'+index+'" quespaper="'+pinpoint[1]+'" quesnumber="'+pinpoint[2]+'" type="radio" name="qopt" value="'+option+'" class="opt'+option+'">'+options[option]+'</label></div>';
                }
                $('.Qoptions').html(options_content);
                if(index in answers){
                        $(".opt"+answers[index]["selected"]).prop("checked", true);
                }
        });
    }
};

var numQ = function(Qid,callback){
    $.getJSON( "/Qnumbers/"+Qid, function( data ) {
        callback(data.reasoning,data.english,data.quant,data.ga,data.cs);
    })
}


$(document).ready(function() {
    var Qid = $(".Qidval").attr("Qid");
    numQ(Qid,function(a,b,c,d,e){
        reasoning=a;
        eng=b;
        quant=c;
        ga=d;
        cs=e;
        getQ(Qid,1,[]);
        $("#cover").hide();
    });

    ////// Mark first question as RED
    $("#__Q__1").addClass("ques-wrong");
    

});


