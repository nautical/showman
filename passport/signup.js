var LocalStrategy   = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var crypto = require('crypto');
var app = require('../app');

module.exports = function(passport){

	passport.use('signup', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {

            findOrCreateUser = function(){
                User.findOne({ 'username' :  username }, function(err, user) {
                    if (err){
                        console.log('Error in SignUp: '+err);
                        return done(err);
                    }
                    if (user) {
                        console.log('User already exists with username: '+username);
                        return done(null, false, req.flash('message','User Already Exists'));
                    } else {
                        var newUser = new User();
                        var kypt = crypto.randomBytes(10).toString('hex');
                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.email = req.param('email');
                        newUser.firstName = req.param('firstName');
                        newUser.lastName = req.param('lastName');
                        newUser.working_status = req.param('working');
                        newUser.avg_score = 0;
                        newUser.verification_code = kypt;
                        newUser.is_verified = false;
                        newUser.ibps_accnt_type = "basic";
                        newUser.activation_date = new Date();
                        console.log(newUser);
                        newUser.save(function(err,newUser) {
                            if (err){
                                console.log('Error in Saving user: '+err);  
                                throw err;
                            }
                            else{
                                app.mailer.send(
                                      {
                                        template: 'mail/singup'
                                      },
                                      {
                                        to: newUser.email,
                                        subject: 'Welcome to IBPS.how',
                                        verificationlink: 'http://exam.ibps.how/activateuser/'+newUser._id+'/'+kypt
                                      },
                                      function (err) {
                                        if (err) {
                                          console.log("error sending email" + err + " > " + newUser._id); 
                                        };
                                          console.log("mail sent");
                                      }
                                );
                            }
                            return done(null, newUser);
                        });
                    }
                });
            };
            process.nextTick(findOrCreateUser);
        })
    );

    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}