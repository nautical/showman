var LocalStrategy   = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');

var request = require('request');
var querystring = require('querystring');

module.exports = function(passport){

	passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {
            User.findOne({ 'username' :  username }, 'id is_verified username user_sectional_history password email firstName lastName address user_test_history user_response_history avg_score ibps_accnt_type activation_date' ,
                function(err, user) {
                    if (err)
                        return done(err);
                    if (!user){
                        console.log('User Not Found with username '+username);
                        return done(null, false, req.flash('message', 'User Not found.'));                 
                    }
                    if (!isValidPassword(user, password)){
                        console.log('Invalid Password');
                        return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                    }
                    if(!user.is_verified){
                        console.log('User not verified');
                        return done(null, false, req.flash('message', 'An email was sent to you \
                        with verification link , Please verify your email')); // redirect back to login page
                    }
                    var form = {
                        username: username,
                        password: password,
                        email: user.email
                    };

                    var formData = querystring.stringify(form);
                    var contentLength = formData.length;
                    request({
                        headers: {
                          'Content-Length': contentLength,
                          'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        uri: 'http://localhost:4567/__createUserAPI__',
                        body: formData,
                        method: 'POST'
                      }, function (err, res, body) {
                            return done(null, user);
                      });
                }
            );

        })
    );


    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }
    
}