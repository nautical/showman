var express = require('express');
var router = express.Router();
var controllers = require('../controllers/index')
var config = require('../config.json')
var lpapers = require('../config/lastyear.json')

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated()){
		var admins = config.adminname.split(" ")
		if(config.adminname.indexOf(req.user.username)>=0 ){
			req.user.isadmin = true;
		}
		if(req.user.is_verified)
			return next();
		else{
			req.logout();
			req.flash('message', 'An email has been sent to your email Id with activation link , kindly click on the link to activate');
			res.redirect('/');
		}
	}
	res.redirect('/');
}

var isAuthenticatedRedHome = function (req, res, next) {
	if (req.isAuthenticated()){
		if(req.user.is_verified){
			controllers.renderhome(req,res);
			return;
			//res.redirect('/home');
		}
		else{
			req.logout();
			req.flash('message', 'An email has been sent to your email Id with activation link , kindly click on the link to activate');
			res.redirect('/');
		}
	}
	return next();
}

var isAdmin = function(req, res, next) {
	if (req.isAuthenticated()){
		if(req.user.username === config.adminname){
			req.user.isadmin = true;
			return next();
		}
	}
	res.redirect("/");
}

module.exports = function(passport){

	router.get('/', isAuthenticatedRedHome, function(req, res) {
		res.render('index', { message: req.flash('message') });
	});

	router.get('/login', isAuthenticatedRedHome , function(req, res) {
		res.render('index', { message: req.flash('message') });
	});

	router.post('/login', passport.authenticate('login', {
		successRedirect: '/home',
		failureRedirect: '/',
		failureFlash : true  
	}));

	router.get('/signup', function(req, res){
		res.render('register',{message: req.flash('message')});
	});

	router.post('/signup', passport.authenticate('signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash : true  
	}));

	router.get('/home', isAuthenticated, controllers.renderhome);

	router.get('/activateuser/:id/:code', controllers.activateuser);
	router.get('/forgotpassword/:username' , controllers.forgotpassword);
	router.get('/resetpassword/:id/:code', controllers.resetpassword);
	router.post('/resetpassword', controllers.post_resetpassword);
	router.get('/tools',controllers.getTools);
	router.get('/cutoffs', isAuthenticated, function(req, res){
		res.render('cutoffs', { user: req.user , activetab: "cutoff"});
	});

	router.get('/resendemailverificationcode/:id/:enc',controllers.resendVeri);

	router.get('/launch/:id', isAuthenticated, function(req, res){
		res.render('launch', { user: req.user, eID: req.params.id });
	});

	router.get('/analysis', isAuthenticated, controllers.renderAna);
	router.get('/special', isAuthenticated, controllers.renderSpecial);

	router.get('/papers', isAuthenticated, function(req, res){
		res.render('papers', { user: req.user, activetab: "papers", lyear : lpapers});
	});

	router.get('/profile', isAuthenticated, function(req, res){
		res.render('profile', { user: req.user, message: req.flash('message') , activetab: "profile"});
	});

	router.post('/subscribe', controllers.subscriberAdd);

	router.post('/profilebasic', isAuthenticated, controllers.updateUser);

	router.get('/quiz/:id', isAuthenticated, controllers.renderquiz);
	router.post('/quiz/:id', isAuthenticated, controllers.processquiz);


	router.get('/Qnumbers/:id', isAuthenticated, controllers.quesNum);

	router.get('/quiz/:id/:subject/:ind', isAuthenticated, controllers.quizapi);

	router.get('/top', isAuthenticated, controllers.showtops);

	router.post('/addexamdate', isAdmin, controllers.addexamdate);

	router.get('/addtop', isAdmin, controllers.renderaddtop);

	router.post('/addtop', isAdmin , controllers.updatetops);

	router.get('/deletepost/:id', isAdmin , controllers.deletepost);

	router.get('/deleteall', isAdmin , controllers.deleteall);

	router.get('/createtest', isAdmin, function(req, res){
		res.render('createtest', { user: req.user , activetab: "createtest"});
	});

	router.post('/createtest', isAdmin, controllers.createtestQ);
	router.get('/managetest', isAdmin, controllers.managetestQ);
	router.get('/managetest/:id', isAdmin, controllers.showQuestionstestQ);
	router.post('/managetest/:id', isAdmin, controllers.modtestQ);
	router.get('/manageuser',isAdmin,controllers.manageuser);
	router.get('/getQs/:id', isAdmin, controllers.getQs);
	router.get('/result/:id', isAuthenticated, controllers.getresult);
	router.get('/static/:id', isAuthenticated, controllers.getStaticQ);

	router.post('/static/:id', isAuthenticated, controllers.postStaticQ);
	
	router.get('/static', isAuthenticated, controllers.getStaticQD);
	router.get('/sendmail',isAdmin, controllers.sendmailAdmin);

	router.get('/signout', function(req, res) {
		req.logout();
		req.flash('message','');
		res.redirect('/');
	});

	return router;
}





