var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var minify = require('express-minify');
var dbConfig = require('./db');
var mongoose = require('mongoose');
var mailer = require('express-mailer');

mongoose.connect(dbConfig.url);

var app = module.exports = express();
app.use(compression());

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://ibps.how');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}


app.use(function (req, res, next) {
  res.header("X-Powered-By", "nautical-network-inc")
  res.header("shakeyourbootiya","Shake it shake it")
  next()
})

// app.use(minify({
//   cache: __dirname + '/cache'
// }));

app.set('views', path.join(__dirname, 'views/html'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


// mail config

mailer.extend(app, {
  from: 'no-reply@ibps.how',
  host: 'smtp.gmail.com', // hostname 
  secureConnection: true, // use SSL 
  port: 465, // port for secure SMTP 
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts 
  auth: {
    user: 'webmaster@ibps.how',
    pass: 'Piyush123!'
  }
});

//


app.use(favicon());
app.use(logger('dev'));

app.use(bodyParser({limit: '50mb'}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(allowCrossDomain);
app.use(express.static(path.join(__dirname, '/public')));

var passport = require('passport');
var expressSession = require('express-session');
app.use(expressSession({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());

var flash = require('connect-flash');
app.use(flash());

var initPassport = require('./passport/init');
initPassport(passport);

var routes = require('./routes/index')(passport);
app.use('/', routes);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
